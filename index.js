const express = require('express');
const bodyParser = require('body-parser');

const PORT = 8080;

const app = express();

app.listen(PORT, () => {
          console.log('Server listening on port: ' + PORT);
});

app.get("/", (req, res) => {

            let data = {
                            name: "GFG",
                            age: 33,
                            male: true
                        }

            res.send(data);
});
